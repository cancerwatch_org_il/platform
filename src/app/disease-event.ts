import {Reporter} from "./reporter";
import {Location} from "./location";
export interface DiseaseEvent {
  subType:string,
  cancerType: string,
  otherType: string,
  date: string,
  description:string,
  reporter: Reporter
}
