import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalComponent} from "ng2-bs3-modal/ng2-bs3-modal";
import {AngularFire, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2';
import {MapService} from "../map.service";
import {Haifa} from "./haifa";
import {OtherEvent} from "../other-event";
import {IncidentEvent} from "../incident-event";
import {DiseaseEvent} from "../disease-event";
import {Neighborhood} from "../neighborhood";
import {LatLng} from "../lat-lng";
import {CWGeneral} from "../cwgeneral";
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/take'
import {NeighborhoodsService} from "../neighborhoods.service";


export interface NeighborhoodState {
  id:number,
  cancerCount:number,
  incidentCounts:number,
  otherCount:number
}

class NeighborhoodProperty {
  id:number;
  name_heb:string;
  name_en:string;
  path:any[];
  center:LatLng;

  opacity:number;
  strokeWeight:number;
  strokeOpacity:number;

  constructor(id:number, name_heb:string, name_en:string, path:any[]) {
    this.id = id;
    this.name_heb = name_heb;
    this.name_en = name_en;
    this.path = path;
    this.center = this.getCenter(path);
    this.opacity = 0.2;
    this.strokeWeight = 1;
    this.strokeOpacity = 0.5;

  }


  getCenter(path:any[]) {
    let x_min = 100, y_min = 100, x_max = 0, y_max = 0;
    for (let i = 0; i < path.length; i++) {

      if (x_min > path[i].lat) {
        x_min = path[i].lat;
      }

      if (x_max < path[i].lat) {
        x_max = path[i].lat;
      }

      if (y_min > path[i].lng) {
        y_min = path[i].lng;
      }

      if (y_max < path[i].lng) {
        y_max = path[i].lng;
      }
    }

    return {
      lat: x_min + ((x_max - x_min) / 2),
      lng: y_min + ((y_max - y_min) / 2)
    }

  }
}

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit {

  @ViewChild('modal')
  modal:ModalComponent;

  // Basic map parameters
  center = {
    lat: 32.805,
    lng: 35.060,
    zoom: 13
  };

  // angularfire lists
  generalObj:FirebaseObjectObservable<CWGeneral>;
  general: CWGeneral;
  diseases:FirebaseListObservable<DiseaseEvent[]>;
  incidents:FirebaseListObservable<IncidentEvent[]>;
  others:FirebaseListObservable<OtherEvent[]>;

  neighborhoods:FirebaseListObservable<Neighborhood[]>;
  neighborhoods1: Neighborhood[];
  errorMsg;

  // neighborhoods properties
  neighborhoodProperties:NeighborhoodProperty[] = [];


  address:string;
  selectedEvent = {
    n_id: 0,
    n_name: 'בחר',
    coords: {
      lat: 0,
      lng: 0
    },
    address: ""
  };

  neighborhood: Neighborhood;

  centerMarker = {
    label: "",
    center: null
  }


  constructor(private ms:MapService, private af:AngularFire, private ns: NeighborhoodsService) {

    let generalObj = af.database.object('/general', {preserveSnapshot: true});
    generalObj.subscribe(snapshot => {
      this.general = snapshot.val();
    })
    this.diseases = af.database.list('/diseases');

    this.incidents = af.database.list('/incidents', {
      query:{
        limitToLast: 5,
        orderByKey: true
      }
    });


    this.others = af.database.list('others')
    this.neighborhoods = af.database.list('/neighborhoods');


    for (let h of new Haifa().haifa) {
      this.neighborhoodProperties.push(new NeighborhoodProperty(h.id, h.name_heb, h.name_en, h.path))
    }

  }

  ngOnInit() {

    this.ns.fetchNeighborhoods().subscribe(
      data => this.neighborhoods1 = data,
      error => this.errorMsg = <any>error
        );

/*
    this.general.set({
      total_diseases: 1,
      max_disease: 1,

      total_cancers: 0,
      max_cancer: 0,

      total_incidents: 0,
      max_incident: 0,

      total_others: 0,
      max_other: 0
    });
*/
    // This is for pushing one time DB - do not delete yet
    /*
     for (let i = 0; i < this.haifa.length; i++) {
     this.neighborhoods.push({
     id: Number(this.haifa[i].id),
     name_heb: this.haifa[i].name_heb,
     name_en: this.haifa[i].name_en,

     disease_count: 0,
     cancer_count: 0,
     incident_count: 0,
     other_count: 0

     })
     }
     */
  }

  onFabClick() {
    this.address = "";
    this.modal.open()

  }

  submit() {
    this.modal.close();
  }


  // ------------------ //
  // -- POLY HANDLER -- //
  // ------------------ //

  pClick(id, event) {
    let coords = {
      lat: event.latLng.lat(),
      lng: event.latLng.lng(),
    }
    this.ms.getGeoCode(coords)
      .subscribe(
        (address:string) => {
          this.selectedEvent = {
            n_id: id,
            n_name: this.neighborhoodProperties[id - 1].name_heb,
            coords: coords,
            address: address
          }

        });
    this.modal.open();
  }

  pRightClick(id) {

  }

  pMouseOver(id:number) {
    //this.haifa[id - 1].properties.color = "#4286f4";
    this.neighborhoodProperties[id - 1].opacity = 0.8;
    this.neighborhoodProperties[id - 1].strokeWeight = 2;
    this.neighborhoodProperties[id - 1].strokeOpacity = 1;

    this.centerMarker.label = this.neighborhoodProperties[id - 1].name_heb;
    this.centerMarker.center = this.neighborhoodProperties[id - 1].center;

    this.af.database.list('/neighborhoods', {
      query: {
        orderByChild: 'id',
        equalTo: id
      }
    }).subscribe(
      items =>{
        this.neighborhood = items[0];
      }
    )
  }

  pMouseOut(id:number) {
    this.neighborhoodProperties[id - 1].opacity = 0.2;
    this.neighborhoodProperties[id - 1].strokeWeight = 1;
    this.neighborhoodProperties[id - 1].strokeOpacity = 0.5;
    this.centerMarker.label = "";
    this.centerMarker.center = null;
  }


  // Helpers:

  getPath(id:number) {
    return this.neighborhoodProperties[id - 1].path;
  }


  getColor(id:number, obj:any) {
    let diseasesCount = 0;
    let diseasesMax = 10;
    if (obj){
      diseasesCount = Object.keys(obj).length;
      if (diseasesCount > diseasesMax)
        diseasesCount = diseasesMax;
    }

    let hue = ((1 - ( diseasesCount / 10)) * 120).toString(10);
    return ["hsl(", hue, ",100%,50%)"].join("");
  }

  getOpacity(id:number) {
    return this.neighborhoodProperties[id - 1].opacity;

  }

  getStrokeWeight(id:number) {
    return this.neighborhoodProperties[id - 1].strokeWeight;

  }

  getStrokeOpacity(id:number) {
    return this.neighborhoodProperties[id - 1].strokeOpacity;

  }


  // Deprecated:

  onMapClick(event) {

    let coords = {
      lat: event.coords.lat,
      lng: event.coords.lng
    }

    this.ms.getGeoCode(coords)
      .subscribe(
        (address:string) => {
          this.selectedEvent = {
            n_id: 0,
            n_name: "שכונה לא ידועה",
            coords: coords,
            address: address
          }

        });
  }


}
