export interface Reporter {
  anonymous: boolean
  name: string,
  phone: string,
  email: string
}
