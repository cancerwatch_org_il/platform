import {DiseaseEvent} from "./disease-event";
import {IncidentEvent} from "./incident-event";
import {OtherEvent} from "./other-event";
export interface Neighborhood {
  id: number,
  name_heb: string,
  name_en: string,
  disease_count: number,
  cancer_count: number,
  incident_count: number,
  diseases: DiseaseEvent[],
  cancers: DiseaseEvent[],
  incidents: IncidentEvent[],
  others: OtherEvent[]
  
}
