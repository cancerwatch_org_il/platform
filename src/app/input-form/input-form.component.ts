import {
  Component, OnInit, Input, OnChanges, ViewChild, ElementRef, Output,
  EventEmitter
} from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import {Neighborhood} from "../neighborhood";
import {DiseaseEvent} from "../disease-event";
import {IncidentEvent} from "../incident-event";
import {OtherEvent} from "../other-event";

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})

export class InputFormComponent implements OnInit, OnChanges {

  @ViewChild('form-date') el:ElementRef;

  @Input() eventDetails;

  @Output() submit:EventEmitter<any> = new EventEmitter();

  neighborhoods:FirebaseListObservable<any>;
  diseases:FirebaseListObservable<any>;
  incidents:FirebaseListObservable<any>;
  others:FirebaseListObservable<any>;

  eventForm:FormGroup;

  incidentSelected = false;
  cancerSelected = false;

  anonymousReport = false;

  now;

  selectedIncidentOther:boolean = false;

  selectedMainType:number = 0;

  mainTypeOptions = [
    {
      value: 'disease',
      description: 'אירוע תחלואה'
    },
    {
      value: 'incident',
      description: 'אירוע זיהום אוויר'
    },
    {
      value: 'other',
      description: 'אחר'
    }
  ]

  subTypeDiseaseOptions = [
    {
      value: 'disease1',
      description: 'סרטן - החלמה מלאה'
    },
    {
      value: 'disease2',
      description: 'סרטן - תוך כדי מאבק'
    },
    {
      value: 'disease3',
      description: 'סרטן - מקרה מוות'
    },
    {
      value: 'disease4',
      description: 'אסטמה'
    },
    {
      value: 'disease5',
      description: 'מחלת ריאות חסימתית כרונית (COPD)'
    },
    {
      value: 'disease6',
      description: 'התפתחות עוברים'
    },
    {
      value: 'disease-other',
      description: 'תחלואה - אחר'
    }
  ];

  subTypeIncidentOptions = [
    {
      value: 'pollution1',
      description: 'זיהום אוויר - לפיד בוער'
    },
    {
      value: 'pollution2',
      description: 'זיהום אוויר - עשן'
    },
    {
      value: 'pollution3',
      description: 'זיהום אוויר - ריח רע'
    },
    {
      value: 'pollution4',
      description: 'זיהום רעש'
    },
    {
      value: 'pollution-other',
      description: 'אירוע זיהום אוויר - אחר'
    },
  ]


  neighborhoodsArr:any;

  // Neighborhoods:
  neighborhoodOptions = [
    {
      id: "1",
      name_heb: "קרית שמואל",
      name_en: "kiriat shmuel",
    },

    {
      id: "2",
      name_heb: "בת גלים",
      name_en: "bat galim",
    },

    {
      id: "3",
      name_heb: "אזור תעשיה החדש",
      name_en: "ezor taasiya hadash",
    },

    {
      id: "4",
      name_heb: "נמל חיפה",
      name_en: "nemal haifa",
    },

    {
      id: "5",
      name_heb: "עין הים",
      name_en: "ein hayam",
    },

    {
      id: "6",
      name_heb: "קרית חיים מזרחית",
      name_en: "kiriat hayim mizrahit",
    },

    {
      id: "7",
      name_heb: "קרית אליהו",
      name_en: "kiriat eliyahu",
    },

    {
      id: "8",
      name_heb: "סטלה מריס",
      name_en: "stella maris",
    },

    {
      id: "9",
      name_heb: "קרית אליעזר",
      name_en: "kiriat eliezer",
    },

    {
      id: "10",
      name_heb: "כרמל צרפתי",
      name_en: "carmel zarfati",
    },

    {
      id: "11",
      name_heb: "שער העליה",
      name_en: "shar aliya",
    },

    {
      id: "12",
      name_heb: "רמת שאול",
      name_en: "ramat shaul",
    },

    {
      id: "13",
      name_heb: "מושבה גרמנית",
      name_en: "moshava germanit",
    },

    {
      id: "14",
      name_heb: "קרית שפרינצק",
      name_en: "kiriat sprintzak",
    },

    {
      id: "15",
      name_heb: "עיר תחתית",
      name_en: "eir tahtit",
    },

    {
      id: "16",
      name_heb: "שכונת מאי",
      name_en: "shcunat mai",
    },

    {
      id: "17",
      name_heb: "אזור תעשיה קישון",
      name_en: "ezor tahasia kishon",
    },

    {
      id: "18",
      name_heb: "אזור תעשיה הישן",
      name_en: "ezor tahasia yashan",
    },

    {
      id: "19",
      name_heb: "עבאס",
      name_en: "abas",
    },

    {
      id: "20",
      name_heb: "ואדי נסנאס",
      name_en: "wadi nisnas",
    },

    {
      id: "21",
      name_heb: "קרית רבין",
      name_en: "kiriat rabin",
    },

    {
      id: "22",
      name_heb: "כרמל מערבי",
      name_en: "carmel maaravi",
    },

    {
      id: "23",
      name_heb: "כרמל צפוני",
      name_en: "carmel tzfoni",
    },

    {
      id: "24",
      name_heb: "הדר הכרמל",
      name_en: "hadar carmel",
    },

    {
      id: "25",
      name_heb: "נווה דוד",
      name_en: "neve david",
    },

    {
      id: "26",
      name_heb: "אזור תעשיה חוף שמן",
      name_en: "ezor taashiya hof shemen",
    },

    {
      id: "27",
      name_heb: "ואדי סאליב",
      name_en: "wadi shalib",
    },

    {
      id: "28",
      name_heb: "הדר עליון",
      name_en: "hadar eliyon ",
    },

    {
      id: "29",
      name_heb: "כבביר",
      name_en: "kababir",
    },

    {
      id: "30",
      name_heb: "קיבוץ גלויות",
      name_en: "kibuz galuyot",
    },

    {
      id: "31",
      name_heb: "יפה נוף",
      name_en: "yefe nof",
    },

    {
      id: "32",
      name_heb: "מבואות דרומיים",
      name_en: "mevoat dromiyim",
    },

    {
      id: "33",
      name_heb: "כרמל מרכזי",
      name_en: "carmel merchazi",
    },

    {
      id: "34",
      name_heb: "כרמל ותיק",
      name_en: "carmel vatik",
    },

    {
      id: "35",
      name_heb: "גאולה",
      name_en: "geola",
    },

    {
      id: "36",
      name_heb: "רמת הדר",
      name_en: "ramat hadar",
    },

    {
      id: "37",
      name_heb: "רמת ויז'ניץ",
      name_en: "ramat viznitzh",
    },

    {
      id: "38",
      name_heb: "חליסה",
      name_en: "halisa",
    },

    {
      id: "39",
      name_heb: "כרמליה",
      name_en: "carmelia",
    },

    {
      id: "40",
      name_heb: "תל עמל",
      name_en: "tel amal",
    },

    {
      id: "41",
      name_heb: "נוה פז",
      name_en: "neve paz",
    },

    {
      id: "42",
      name_heb: "אזור תעשיה צ'ק פוסט",
      name_en: "ezor taashiya check post",
    },

    {
      id: "43",
      name_heb: "ורדיה",
      name_en: "vardia",
    },

    {
      id: "44",
      name_heb: "יזרעאליה",
      name_en: "yesraelia",
    },

    {
      id: "45",
      name_heb: "נוה יוסף",
      name_en: "neve yosef",
    },

    {
      id: "46",
      name_heb: "נוה שאנן",
      name_en: "neve shaanan",
    },

    {
      id: "47",
      name_heb: "שמבור",
      name_en: "shambur",
    },

    {
      id: "48",
      name_heb: "רוממה",
      name_en: "rommema",
    },

    {
      id: "49",
      name_heb: "אחוזה",
      name_en: "ahuza",
    },

    {
      id: "50",
      name_heb: "רמת ספיר",
      name_en: "ramat shapir",
    },

    {
      id: "51",
      name_heb: "רמת חן",
      name_en: "ramat hen",
    },

    {
      id: "52",
      name_heb: "שכונת זיו",
      name_en: "schunat ziv",
    },

    {
      id: "53",
      name_heb: "רמת בן גוריון",
      name_en: "ramat ben gurion ",
    },

    {
      id: "54",
      name_heb: "גבעת דאונס",
      name_en: "givat dounes",
    },

    {
      id: "55",
      name_heb: "רמת רמז",
      name_en: "ramat remez",
    },

    {
      id: "56",
      name_heb: "רמת עופר",
      name_en: "ramat ofer",
    },

    {
      id: "57",
      name_heb: "רמת בגין",
      name_en: "ramat begin",
    },

    {
      id: "58",
      name_heb: "רמת אשכול",
      name_en: "ramat eshkol",
    },

    {
      id: "59",
      name_heb: "קרית הטכניון",
      name_en: "kiriat technion",
    },

    {
      id: "60",
      name_heb: "רמת אלון",
      name_en: "ramat alon",
    },

    {
      id: "61",
      name_heb: "רמת גולדה",
      name_en: "ramat golda",
    },

    {
      id: "62",
      name_heb: "רמת אלמוגי",
      name_en: "ramat almugi",
    },

    {
      id: "63",
      name_heb: "סביוני הכרמל",
      name_en: "shavyoni hacamel",
    },

    {
      id: "64",
      name_heb: "הוד הכרמל",
      name_en: "hod hacamel",
    },

    {
      id: "65",
      name_heb: "רמת אבא חושי",
      name_en: "ramat abba hushi",
    }

  ]

  constructor(private af:AngularFire) {
    this.eventForm = new FormGroup({
      'mainType': new FormControl('', Validators.required),
      'neighborhood': new FormControl('', Validators.required),

      // subType - Disease
      'subTypeDisease': new FormControl(''),
      'cancerType': new FormControl(''),
      'otherType': new FormControl(''),
      'diseaseDate': new FormControl(''),
      'diseaseDescription': new FormControl(''),

      // subType - Incident
      'subTypeIncident': new FormControl(''),
      'incidentDescription': new FormControl(''),
      'otherIncidentType': new FormControl(''),

      // subType - Other
      'otherDescription': new FormControl(''),

      // reporter
      'anonymous': new FormControl(),
      'name': new FormControl(''),
      'email': new FormControl(''),
      'phone': new FormControl(''),

      // deprecate:
      'address': new FormControl('')


    })

    this.neighborhoods = af.database.list('/neighborhoods');
    this.diseases = af.database.list('/disease');
    this.incidents = af.database.list('/incidents');
    this.others = af.database.list('/others');

  }

  ngOnInit() {
    this.now = Date.now();
  }

  ngOnChanges() {

    this.eventForm.controls['address'].setValue(this.eventDetails.address);
    this.eventForm.controls['neighborhood'].setValue(this.eventDetails.n_id);
  }


  // ---------------------- //
  // -- Select Main Type -- //
  // ---------------------- //

  onSelectTypeMain(selectID) {

    switch (selectID) {
      case 'disease':
        this.selectedMainType = 1;
        break;
      case 'incident':
        this.selectedMainType = 2;
        break;
      case 'other':
        this.selectedMainType = 3;
        break;
      default:
        break;
    }
  }


  // ----------------------------- //
  // -- Select Sub Type Disease -- //
  // ----------------------------- //

  onDiseaseSelect(selectID) {

    switch (selectID) {
      case 'disease1':
      case 'disease2':
      case 'disease3':
        this.cancerSelected = true;
        break;
      default:
        this.cancerSelected = false;
    }


  }

  onIncidentSelect(selectID) {
    if (selectID == 'pollution-other') {
      this.selectedIncidentOther = true;
    } else {
      this.selectedIncidentOther = false;
    }
  }

  onSelectOther(selectID) {

  }

  onSelectedNeighborhood(id) {
    this.eventDetails.n_id = id;

  }


  onSubmit() {

    console.log(this.eventForm)
    let d = new Date();
    switch (this.eventForm.value['mainType']) {

      case 'disease':

        let newDisease:DiseaseEvent = {
          subType: this.eventForm.value['subTypeDisease'],
          cancerType: this.eventForm.value['cancerType'],
          otherType: this.eventForm.value['otherType'],
          //date: this.eventForm.value['diseaseDate'],
          date:  d.toLocaleString(),
          description: this.eventForm.value['diseaseDescription'],
          reporter: {
            anonymous: this.eventForm.value['anonymous'],
            name: this.eventForm.value['name'],
            phone: this.eventForm.value['phone'],
            email: this.eventForm.value['email']
          }
        }

        this.af.database.list('/neighborhoods/' + String(Number(this.eventDetails.n_id) - 1) + "/diseases").push(newDisease);
        switch (this.eventForm.value['subTypeDisease']) {
          case 'disease1':
          case 'disease2':
          case 'disease3':
            this.af.database.list('/neighborhoods/' + String(Number(this.eventDetails.n_id) - 1) + "/cancers").push(newDisease);
            break;
          default:
            this.cancerSelected = false;
        }
        break;

      case 'incident':

        let newIncident:IncidentEvent = <IncidentEvent>{
          subType: this.eventForm.value['subTypeIncident'],
          otherType: this.eventForm.value['otherIncidentType'],
          description: this.eventForm.value['incidentDescription'],
          location: {
            neighborhood: {
              id: Number(this.eventDetails.n_id),
              name_heb: this.neighborhoodOptions[Number(this.eventDetails.n_id) - 1].name_heb,
              name_en: '',
              disease_count: 0,
              cancer_count: 0,
              incident_count: 0,
              diseases: null
            },
            city: '',
            street: '',
            latLng: this.eventDetails.coords
          },
          reporter: {
            anonymous: this.eventForm.value['anonymous'],
            name: this.eventForm.value['name'],
            phone: this.eventForm.value['phone'],
            email: this.eventForm.value['email']
          },
          time: d.toLocaleString()
        }

        this.af.database.list('/neighborhoods/' + String(Number(this.eventDetails.n_id) - 1) + "/incidents").push(newIncident);
        this.incidents.push(newIncident)
        break;

      case 'other':

        let newOther:OtherEvent = {
          description: this.eventForm.value['otherDescription'],
          reporter: {
            anonymous: this.eventForm.value['anonymous'],
            name: this.eventForm.value['name'],
            phone: this.eventForm.value['phone'],
            email: this.eventForm.value['email']
          }
        }
        this.others.push(newOther);
        this.af.database.list('/neighborhoods/' + String(Number(this.eventDetails.n_id) - 1) + "/others").push(newOther);
        break;
    }


    this.submit.emit(null);
    this.eventForm.reset();
  }


}

/*
 $('#sandbox-container input').datepicker({
 format: "dd/mm/yyyy",
 endDate: "today",
 todayBtn: "linked",
 language: "he",
 autoclose: true,
 todayHighlight: true
 });

 */
