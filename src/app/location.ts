import {LatLng} from "./lat-lng";
import {Neighborhood} from "./neighborhood";
export interface Location {
  neighborhood: Neighborhood,
  city: string,
  street: string,
  latLng: LatLng
}
