export interface CWGeneral {
  total_diseases: number;
  max_disease: number;
  
  total_cancers: number;
  max_cancer: number;
  
  total_incidents: number;
  max_incident: number;
  
  total_others: number;
  max_other: number;
  
}
