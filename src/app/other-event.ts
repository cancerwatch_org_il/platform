import {Reporter} from "./reporter";
export interface OtherEvent {
  description: string,
  reporter: Reporter
}
