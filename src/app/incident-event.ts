import {Reporter} from "./reporter";
import {Location} from "./location";
export interface IncidentEvent {
  subType: string,
  otherType: string,
  description: string,
  location: Location,
  reporter: Reporter,
  time: string
}
