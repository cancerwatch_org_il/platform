import {Component, OnInit, Input} from '@angular/core';
import {Neighborhood} from "../neighborhood";

@Component({
  selector: 'app-neighborhood-details',
  templateUrl: './neighborhood-details.component.html',
  styleUrls: ['./neighborhood-details.component.css']
})
export class NeighborhoodDetailsComponent implements OnInit {

  @Input() neighborhood:Neighborhood;
  @Input() rank:number;

  diseases_count:number = 0;
  cancers_count:number = 0;
  incidents_count:number = 0;
  others_count:number = 0;

  constructor() {
    
  }


  ngOnInit() {
    if (this.neighborhood) {
      if (this.neighborhood.diseases)
        this.diseases_count = Object.keys(this.neighborhood.diseases).length;
      if (this.neighborhood.cancers)
        this.cancers_count = Object.keys(this.neighborhood.cancers).length;
      if (this.neighborhood.incidents)
        this.incidents_count = Object.keys(this.neighborhood.incidents).length;
      if (this.neighborhood.others)
        this.others_count = Object.keys(this.neighborhood.others).length;
    }
  }
}
